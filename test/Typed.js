const { ethers } = require("hardhat")
const { expect } = require("chai");

describe("Typed", () => {
  it("should return correct signer", async () => {
    const signer = ethers.Wallet.createRandom()
    const type = {
      Hash: [
        {name: "seed", type: "uint256"},
        {name: "text", type: "string"},
      ]
    }
    const typed = await (await ethers.getContractFactory("Typed")).deploy()
    const domain = {
      name: "Typed",
      version: "1.0",
      chainId: (await ethers.provider.getNetwork()).chainId,
      verifyingContract: typed.address,
    }
    const message = {
      seed: 0,
      text: "Hello"
    }
    const signature = await signer._signTypedData(domain, type, message)
    const contractSigner = await typed.getSigner(message, signature)
    expect(contractSigner).to.be.equal(signer.address)
  })
})