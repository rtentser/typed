// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;
import "@openzeppelin/contracts/utils/cryptography/EIP712.sol";

contract Typed is EIP712 {
    using ECDSA for bytes32;

    constructor() EIP712("Typed", "1.0") {}

    bytes32 private constant _TYPEHASH =
        keccak256("Hash(uint256 seed,string text)");

    struct Hash {
        uint256 seed;
        string text;
    }

    function getSigner(
        Hash calldata message,
        bytes calldata signature
    ) external view returns (address) {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        _TYPEHASH,
                        message.seed,
                        keccak256(bytes(message.text))
                    )
                )
            ).recover(signature);
    }
}
